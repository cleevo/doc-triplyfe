
===========================
Travel Organizer
===========================

Registrasi sebagai Tour Operator / Tour Organizer
===================================

Platform TripLyfe ID memberi peluang kepada pengguna untuk menjangkau pelanggan yang lebih luas dengan membuak akses ke pasar pengguna 
TripLyfe dan juga memberikan tools yang diperlukan untuk mengelola sebuah Tour Organizer.

Berikut beberapa langkah yang dilakukan untuk mendaftar menjadi Tour Organizer :

- Buka browser, masukkan alamat url: https://triplyfe.id/joinOrganizer

.. image:: images/joinOrganizer.jpg
   :align: center
   :width: 600
|

- Pada menu yang muncul, isi field-field yang ada sesuai dengan data yang Anda miliki. 
  Parameters:

   - ``Organizer Name``: Isi dengan nama entitas tour organizer Anda;
   - ``Product Type``: ``"Open Trip"``, ``"Car Rent"``, ``"Hotel/Villa"``, ``"Ticketing"`` Pilih kategori usaha yang ditawarkan;
   - ``Email Address``: Isi dengan alamat email;
   - ``Phone``: Isi dengan nomor telephone aktif;

- Setelah selesai mengisi field-field yang diperlukan, klik tombol :guilabel:`SUBMIT`.

- Demi kepentingan pengguna, untuk menghindari kemungkinan Anda didaftarkan oleh orang yang tidak berkepentingan, triplyfe.id akan mengirimkan email konfirmasi melalui 
  alamat email yang telah Tripster masukkan sebelumnya. Klik :guilabel:`Confirm Account`, dengan demikian Anda telah resmi menyandang sebutan Tripster dan berhak atas 
  fitur-fitur yang kami tawarkan.

.. image:: images/confirm.jpg
   :align: center
   :width: 1000
|    

Sign In
=======
Visit https://www.triplyfe.id/organizers/signin

Dashboard Travel Organizer / Tour Operator
==========================================
Setiap Travel Organizer / Tour Operator yang telah melewati verifikasi dari Tim Triplyfe akan mendapatkan akses ke backoffice
yang dapat digunakan untuk melihat dashboard, menambah, mengedit atau menghapus produk, melihat daftar transaksi, mengupdate profile dan juga password.

Halaman Dashboard menampilkan beberapa informasi antara lain:

1. Balance saat ini
2. Pending Orders
3. Finished Order
4. Complain Order


Upload Products
===============
1. Klik menu :guilabel:`Products`` pada menu samping
2. Klik tombol :guilabel:`Add Product`
3. Akan muncul form yang harus diisi

   - ``Category``: ``"Diving Course"``, ``"Trekking"``, ``"Sailing"``, ``"Cycling"``, ``"Trial Running"``, ``"Water Trekking"``, ``"Open Water Diving"``, ``"Rent Car"``, ``"Open Trip"``, ``"Private Trip"``, ``"Hotel"``, ``"Riding"``, ``"Surfing"``, ``"Canyoning"`` Pilih kategori produk yang ditawarkan;
   - ``Region``: Pilih Region
   - ``Title``: Judul Paket Trip yang ditawarkan
   - ``Passport Needed?``: Apakah memerlukan passport untuk paket yang ditawarkan? Jika iya maka customer wajib mengupload salinan paspor setelah melakukan pembayaran
   - ``About Product``: Deskripsi mengenai paket yang ditawarkan dan dibuat narasi yang menarik mengenai trip yang ditawarkan
   - ``Itinerary``: Susunan rencana perjalanan sesuai dengan trip yang ditawarkan
   - ``Term & Conditions``: Syarat-syarat dan kondisi yang mesti dipenuhi oleh kedua belah pihak
   - ``Hotel included``: Nama hotel. Diisi jika ada / hotel diatur oleh Anda.
   - ``Slot``: Jumlah slot yang ditawarkan (diisi hanya jika ada)
   - ``Schedule Type``: ``"Always Available"``, ``"Single Schedule"``, ``"Always Available"``


Mengatur Pesanan
================
1. Klik menu :guilabel:`Transaction` pada menu di kiri
2. Klik tombol :guilabel:`Detail` untuk melihat detail pesanan
3. Klik tombol :guilabel:`Approve Order` jika menerima order tersebut
4. Klik tombol :guilabel:`Process Order` jika order sudah diproses dan customer siap berangkat
5. Klik :guilabel:`Finish Order` jika trip sudah selesai

Memperbarui Profil
==================
1. Klik menu :guilabel:`Profile`` pada menu di kiri
2. Isi form yang tersedia

   - ``Name``: Nama Travel Agent / Tour Operator / Travel Organizer
   - ``Email``: Alamat email
   - ``Bank``: Nama Bank
   - ``Acc Name``: Nama Account
   - ``Acc Number``: Nomor Rekening
   - ``PIC``: Penanggung jawab
   - ``Phone``: Nomor telephone
   - ``Description``: Deskripsi
   - ``Logo``: Logo
   - ``Page Banner``: Image yang ingin ditampilkan di halaman profil

3. Pastikan nomor rekening terisi untuk memudahkan pencairan digunakan
4. Pastikan logo dan background terupload agar profilmu semakin cantik

Membuat paket open trip
=========================
|

Membuat paket private trip
=========================
|

Membuat paket sewa mobil
=========================
|